import requests # requires installation: File > Settings > Project > Python interperter > + > requests > Install
import json

resp = requests.get('https://jsonplaceholder.typicode.com/todos/1')
print(resp)
print(type(resp))
print(resp.status_code)
print(resp.content)
d = json.loads(resp.content)
print(d)
print(d['id'])
print(d.keys())
list_keys = list(d.keys())
print(f'id is in keys? {"id" in list_keys}')
print(f'address is in keys? {"address" in list_keys}')

#resp = requests.get('https://jsonplaceholder.typicode.com/todos/1')
resp = requests.get('https://randomuser.me/api/')
content = json.loads(resp.content)
print(content['results'][0]['name'])

